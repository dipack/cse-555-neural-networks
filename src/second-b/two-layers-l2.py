#!/usr/bin/env python3
import logging
import sys
from collections import defaultdict

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from keras import losses
from keras.callbacks import LambdaCallback
from keras.datasets import mnist
from keras.layers import Dense
from keras.models import Sequential
from keras.optimizers import SGD
from keras.utils import to_categorical
from keras.regularizers import l2 as L2

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)


def build_model(X):
    first_layer_nodes = 30
    second_layer_nodes = 10
    num_features = len(X[0])
    kernel_lambda = 0.5

    model = Sequential()
    model.add(Dense(first_layer_nodes, input_shape=(num_features,), activation='sigmoid',
                    kernel_regularizer=L2(kernel_lambda)))
    model.add(Dense(first_layer_nodes, activation='sigmoid', kernel_regularizer=L2(kernel_lambda)))
    model.add(Dense(second_layer_nodes, activation='softmax'))

    model.summary()

    sgd = SGD(lr=0.1)
    model.compile(optimizer=sgd, loss=losses.mean_squared_error, metrics=['accuracy', 'mse'])
    return model


def train_model(X, y):
    model = build_model(X)

    num_epochs = 30
    model_batch_size = 10

    layer_weights = defaultdict(list)

    def append_weights(layers):
        for idx, layer in enumerate(layers):
            weights, bias = layer.get_weights()
            weights = np.ndarray.flatten(weights, 'C')
            layer_weights[idx].append(weights)
        return

    weights_change_callback = LambdaCallback(on_epoch_end=lambda batch, logs: append_weights(model.layers))
    history = model.fit(X, y, validation_split=0.2, epochs=num_epochs, batch_size=model_batch_size,
                        callbacks=[weights_change_callback])

    layer_weight_differentials = calculate_weight_changes(layer_weights)
    history_df = pd.DataFrame(history.history)
    plot_stuff(history_df, layer_weight_differentials)
    return model


def calculate_weight_changes(layer_weights):
    differentials = dict()
    for layer, weights in layer_weights.items():
        logging.info("Computing weights differential for layer {0}".format(layer))
        differential = np.diff(weights, axis=0)
        avg_differential = np.mean(differential, axis=1)
        differentials[layer] = avg_differential
    return differentials


def plot_stuff(history_df, layer_weight_differentials):
    # Training, Testing Loss
    plt.plot(history_df['loss'])
    plt.plot(history_df['val_loss'])
    plt.title('Model Loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Training', 'Testing'], loc='upper left')
    plt.savefig('graphs/two-layer-l2-loss.png')
    plt.clf()
    plt.cla()
    plt.close()
    # Training, Testing Accuracy
    plt.plot(history_df['acc'])
    plt.plot(history_df['val_acc'])
    plt.title('Model Accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Training', 'Testing'], loc='upper left')
    plt.savefig('graphs/two-layer-l2-accuracy.png')
    plt.clf()
    plt.cla()
    plt.close()
    # Weight changes for each layer
    plt_labels = []
    for layer, diff in layer_weight_differentials.items():
        plt.plot(diff)
        plt_labels.append("Layer {0}".format(layer))
    plt.title('Weight changes by layer')
    plt.ylabel('Weight change')
    plt.xlabel('Epoch')
    plt.legend(plt_labels, loc='upper left')
    plt.savefig('graphs/two-layer-l2-weight_change.png')
    plt.clf()
    plt.cla()
    plt.close()
    return


def pick_samples(X, y, num=100):
    numbers = set(y)
    sampled_x, sampled_y = [], []
    for number in numbers:
        x = X[y == number]
        max_len = min(len(x), num)
        # To ensure that subsequent runs of this function do not always return the same samples
        np.random.shuffle(x)
        sampled_x.extend([xx for xx in x[:max_len]])
        sampled_y.extend([number for _ in range(max_len)])
    sampled_x, sampled_y = np.asarray(sampled_x), np.asarray(sampled_y)
    return sampled_x, sampled_y


def run_neural_network():
    num_classes = 10

    (X_train, y_train), (X_test, y_test) = mnist.load_data()

    X_train, y_train = pick_samples(X_train, y_train, 100)
    X_test, y_test = pick_samples(X_test, y_test, 100)

    X_train = X_train.reshape(X_train.shape[0], X_train.shape[1] * X_train.shape[2]).astype(np.float32)
    X_test = X_test.reshape(X_test.shape[0], X_test.shape[1] * X_test.shape[2]).astype(np.float32)

    y_train_onehot, y_test_onehot = to_categorical(y_train, num_classes), to_categorical(y_test, num_classes)

    model = train_model(X_train, y_train_onehot)

    loss, accuracy, mse = model.evaluate(X_train, y_train_onehot)
    logging.info("MNIST Train Evaluated loss: {0}".format(loss))
    logging.info("MNIST Train Evaluated accuracy: {0}".format(accuracy))

    loss, accuracy, mse = model.evaluate(X_test, y_test_onehot)
    logging.info("MNIST Test Evaluated loss: {0}".format(loss))
    logging.info("MNIST Test Evaluated accuracy: {0}".format(accuracy))

    return


def main():
    run_neural_network()
    return


if __name__ == "__main__":
    main()
