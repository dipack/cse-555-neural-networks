#!/usr/bin/env python3
import logging
import os
from collections import defaultdict

import cv2
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from keras import backend as K
from keras import losses
from keras.callbacks import EarlyStopping, LambdaCallback
from keras.datasets import mnist
from keras.layers import Dense, Dropout, Conv2D, MaxPooling2D, Flatten
from keras.models import Sequential, load_model
from keras.optimizers import RMSprop
from keras.utils import to_categorical
from scipy import ndimage as Img

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)


def build_model(input_shape, num_classes):
    first_layer_nodes = 256

    model = Sequential()
    model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1), activation='relu', input_shape=input_shape))
    model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.1))
    model.add(Flatten())
    model.add(Dense(first_layer_nodes, activation='relu'))
    model.add(Dense(num_classes, activation='softmax'))

    model.compile(loss=losses.categorical_crossentropy,
                  optimizer=RMSprop(),
                  metrics=['accuracy', 'mse'])

    return model


def train_model(X, y, input_shape, num_classes):
    model_filename = "mnist_model.h5"
    if os.path.isfile(model_filename):
        logging.info("Loading existing model from {}".format(model_filename))
        model = load_model(model_filename)
    else:
        model = build_model(input_shape, num_classes)

        validation_data_split = 0.1
        num_epochs = 10
        model_batch_size = 100

        layer_weights = defaultdict(list)

        def append_weights(layers):
            for idx, layer in enumerate(layers):
                try:
                    weights, bias = layer.get_weights()
                    weights = np.ndarray.flatten(weights, 'C')
                    layer_weights[idx].append(weights)
                except ValueError as e:
                    logging.error(e)
                    logging.error(
                        "Error occurred while extracting weights for layer {0} as it most likely does not have any".format(
                            idx))
                    continue
            return

        early_stopper = EarlyStopping(monitor='val_loss', patience=1)
        weights_change_callback = LambdaCallback(on_epoch_end=lambda batch, logs: append_weights(model.layers))

        history = model.fit(X, y,
                            epochs=num_epochs,
                            batch_size=model_batch_size,
                            validation_split=validation_data_split,
                            callbacks=[weights_change_callback, early_stopper]
                            )

        layer_weight_differentials = calculate_weight_changes(layer_weights)
        history_df = pd.DataFrame(history.history)
        plot_stuff(history_df, layer_weight_differentials)

        logging.info("Saving newly trained model to {}".format(model_filename))
        model.save(model_filename)
    return model


def calculate_weight_changes(layer_weights):
    differentials = dict()
    for layer, weights in layer_weights.items():
        logging.info("Computing weights differential for layer {0}".format(layer))
        differential = np.diff(weights, axis=0)
        avg_differential = np.mean(differential, axis=1)
        differentials[layer] = avg_differential
    return differentials


def plot_stuff(history_df, layer_weight_differentials):
    # Training, Testing Loss
    plt.plot(history_df['loss'])
    plt.plot(history_df['val_loss'])
    plt.title('Model Loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Training', 'Testing'], loc='upper left')
    plt.savefig('graphs/loss.png')
    plt.clf()
    plt.cla()
    plt.close()
    # Training, Testing Accuracy
    plt.plot(history_df['acc'])
    plt.plot(history_df['val_acc'])
    plt.title('Model Accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Training', 'Testing'], loc='upper left')
    plt.savefig('graphs/accuracy.png')
    plt.clf()
    plt.cla()
    plt.close()
    # Weight changes for each layer
    plt_labels = []
    for layer, diff in layer_weight_differentials.items():
        plt.plot(diff)
        plt_labels.append("Layer {0}".format(layer))
    plt.title('Weight changes by layer')
    plt.ylabel('Weight change')
    plt.xlabel('Epoch')
    plt.legend(plt_labels, loc='upper left')
    plt.savefig('graphs/weight_change.png')
    plt.clf()
    plt.cla()
    plt.close()
    return


def format_data(X, y=None, num_classes=10):
    # input image dimensions
    img_rows, img_cols = 28, 28

    # For debugging purposes
    # idx, = np.random.randint(0, len(X), 1)
    # shift_rotate_image(X[idx], save_image=True)

    logging.info("Rotating and shifting images")
    X_randomized = np.asarray([shift_rotate_image(x) for x in X])
    logging.info("Done")

    if K.image_data_format() == 'channels_first':
        X_randomized = np.reshape(X_randomized, (X_randomized.shape[0], 1, img_rows, img_cols))
        input_shape = (1, img_rows, img_cols)
    else:
        X_randomized = np.reshape(X_randomized, (X_randomized.shape[0], img_rows, img_cols, 1))
        input_shape = (img_rows, img_cols, 1)

    X_randomized = X_randomized.astype("float32")
    X_randomized /= 255

    if y is not None:
        y = to_categorical(y, num_classes)

    return X_randomized, y, input_shape


def shift_rotate_image(image, save_image=False):
    # The colour for the background is black
    background_value = -0.5
    # Returns array with one value: Angle of rotation
    angle = np.random.randint(-3, 3, 1)
    # Returns array with two values: Shift value along any one axis
    shift = np.random.randint(-3, 3, 2)

    rotated = Img.rotate(image, angle, reshape=False, cval=background_value)
    shifted = Img.shift(rotated, shift, cval=background_value)

    if save_image:
        logging.info("Rotation: {0}, Shift: {1}".format(angle, shift))
        cv2.imwrite("original.png", image)
        cv2.imwrite("rotated.png", shifted)

    return np.asarray(shifted)


def run_neural_network():
    num_classes = 10

    (X_train, y_train), (X_test, y_test) = mnist.load_data()

    logging.info("Formatting images, labels for training data")
    X_train, y_train_onehot, input_shape = format_data(X_train, y_train, num_classes)

    logging.info("Formatting images, labels for testing data")
    X_test, y_test_onehot, _ = format_data(X_test, y_test, num_classes)

    model = train_model(X_train, y_train_onehot, input_shape=input_shape, num_classes=num_classes)

    loss, accuracy, mse = model.evaluate(X_train, y_train_onehot)
    logging.info("MNIST Train Evaluated loss: {0}".format(loss))
    logging.info("MNIST Train Evaluated accuracy: {0}".format(accuracy))

    loss, accuracy, mse = model.evaluate(X_test, y_test_onehot)
    logging.info("MNIST Test Evaluated loss: {0}".format(loss))
    logging.info("MNIST Test Evaluated accuracy: {0}".format(accuracy))

    return


def main():
    run_neural_network()
    return


if __name__ == "__main__":
    main()
