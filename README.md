# CSE 555 - Assignment 4 - Neural Networks
## Create neural networks to classify MNIST numbers
### Instructions
1. To run each of the subquestion related functions, we need the following libraries, `numpy` , `matplotlib`, `keras`, `tensorflow`, `pandas`
2. Switch into each directory, and simply call each of the files as follows: `python3 main.py`
    * Ensure that when you switch into each of the sub-directories, there exists a `graphs/` subfolder, as that is where the networks will dump their respective graphs
    * In the case 2(b), there is no `main.py`; instead of each of the different networks are described in their filenames
